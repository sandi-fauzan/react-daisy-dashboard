import { useState } from "react";
import { Link } from "react-router-dom";
const Sidebar = () => {
    const [currentPage, setCurrentPage] = useState("/");

    return <section id="sidebar" className="h-[100dvh] col-span-2">
        <div className="pr-2 pl-6 my-20 w-full">
            <section id="logo">
                <h2 className="text-[24px]">Daisy Admin</h2>
            </section>
            <section className="mt-10 block w-100" id="navLinks">
                <div className={`items-center block transition duration-200 my-2 px-2 py-3 rounded-full pl-6 ${currentPage == '/' ?'active':''}`}>
                    <Link to="/" onClick={() => setCurrentPage("/")} className="w-full text-[12px]">
                        <i class="fa-solid fa-house mr-2"></i> Dashboard
                    </Link>
                </div>
                <div className={`items-center block transition duration-200 my-2 px-2 py-3 rounded-full pl-6 ${currentPage == '/table' ?'active':''}`}>
                    <Link to="/table" onClick={() => setCurrentPage("/table")} className="w-full text-[12px]">
                        <i class="fa-solid fa-table mr-2"></i> Table
                    </Link>
                </div>
                <div className={`items-center block transition duration-200 my-2 px-2 py-3 rounded-full pl-6 ${currentPage == '/forms' ?'active':''}`}>
                    <Link to="/forms" onClick={() => setCurrentPage("/forms")} className="w-full text-[12px]">
                    <i class="fa-solid fa-list-ul mr-2"></i> Forms
                    </Link>
                </div>
            </section>
        </div>
        <div className="dropdown dropdown-top absolute bottom-0">
            <div tabIndex={0} role="button" className="btn m-1">
                Theme
                <svg width="12px" height="12px" className="h-2 w-2 fill-current opacity-60 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2048 2048"><path d="M1799 349l242 241-1017 1017L7 590l242-241 775 775 775-775z"></path></svg>
            </div>
            <ul tabIndex={0} className="dropdown-content z-[1] p-2 shadow-2xl bg-base-300 rounded-box w-52">
                <li><input type="radio" name="theme-dropdown" className="theme-controller btn btn-sm btn-block btn-ghost justify-start" aria-label="Default" value="default"/></li>
                <li><input type="radio" name="theme-dropdown" className="theme-controller btn btn-sm btn-block btn-ghost justify-start" aria-label="Retro" value="retro"/></li>
                <li><input type="radio" name="theme-dropdown" className="theme-controller btn btn-sm btn-block btn-ghost justify-start" aria-label="Emerald" value="emerald"/></li>
                <li><input type="radio" name="theme-dropdown" className="theme-controller btn btn-sm btn-block btn-ghost justify-start" aria-label="Forest" value="forest"/></li>
                <li><input type="radio" name="theme-dropdown" className="theme-controller btn btn-sm btn-block btn-ghost justify-start" aria-label="Garden" value="garden"/></li>
                <li><input type="radio" name="theme-dropdown" className="theme-controller btn btn-sm btn-block btn-ghost justify-start" aria-label="Luxury" value="luxury"/></li>
                <li><input type="radio" name="theme-dropdown" className="theme-controller btn btn-sm btn-block btn-ghost justify-start" aria-label="Dim" value="dim"/></li>
            </ul>
        </div>
    </section>
}

export default Sidebar;