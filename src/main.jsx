import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './assets/tailwind.css'
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <PerfectScrollbar>
      <App />
    </PerfectScrollbar>
  </React.StrictMode>,
)
