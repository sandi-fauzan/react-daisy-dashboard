import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./Pages/Layout";
import Table from "./Pages/Table";
import Dashboard from "./Pages/Dashboard";
import { Forms } from "./Pages/Forms";

function App() {

  return <BrowserRouter>
  <Routes>
    <Route path="/" element={<Layout />}>
      <Route path="" element={<Dashboard />}></Route>
      <Route path="table" element={<Table />}></Route>
      <Route path="forms" element={<Forms />}></Route>
    </Route>
  </Routes>
  </BrowserRouter>
}

export default App
