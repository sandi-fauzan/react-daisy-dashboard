import { Outlet } from "react-router-dom";
import Sidebar from "../components/Sidebar";

const Layout = () => {
    return <>
        <div className="grid grid-cols-12 gap-4 bg-base-200">
            <Sidebar />
            <section id="mainContent" className="col-span-10 rounded-xl m-8 bg-base-300">
                <div className="m-8">
                    <Outlet />
                </div>
            </section>
        </div>
    </>
} 

export default Layout;