import React from 'react'

export const Forms = () => {
  return (
    <div>
        <h3>Forms</h3>

        <div className="py-4 grid grid-cols-3 gap-6">
            <div className="w-full card border border-base-300">
                <p className='mb-2 text-[12px]'>Text Input</p>
                <input type="text" placeholder="Type here" className="input input-bordered w-full max-w-xs" />
            </div>
            <div className="w-full card border border-base-300">
                <p className='mb-2 text-[12px]'>Textarea</p>
                <textarea className="textarea textarea-bordered" placeholder="Bio"></textarea>
            </div>
            <div className="w-full card border border-base-300">
                <p className='mb-2 text-[12px]'>Select</p>
                <select className="select select-bordered w-full max-w-xs">
                    <option disabled selected>Who shot first?</option>
                    <option>Han Solo</option>
                    <option>Greedo</option>
                </select>
            </div>
            <div className="w-full card border border-base-300">
                <p className='mb-2 text-[12px]'>Rating</p>
                <div className="rating">
                    <input type="radio" name="rating-2" className="mask mask-star-2 bg-orange-400" />
                    <input type="radio" name="rating-2" className="mask mask-star-2 bg-orange-400" checked />
                    <input type="radio" name="rating-2" className="mask mask-star-2 bg-orange-400" />
                    <input type="radio" name="rating-2" className="mask mask-star-2 bg-orange-400" />
                    <input type="radio" name="rating-2" className="mask mask-star-2 bg-orange-400" />
                </div>
            </div>
            <div className="w-full card border border-base-300">
                <p className='mb-2 text-[12px]'>Rating with Luv</p>
                <div className="rating gap-1">
                    <input type="radio" name="rating-3" className="mask mask-heart bg-red-400" />
                    <input type="radio" name="rating-3" className="mask mask-heart bg-orange-400" checked />
                    <input type="radio" name="rating-3" className="mask mask-heart bg-yellow-400" />
                    <input type="radio" name="rating-3" className="mask mask-heart bg-lime-400" />
                    <input type="radio" name="rating-3" className="mask mask-heart bg-green-400" />
                </div>
            </div>
            <div className="w-full card border border-base-300">
                <div className="form-control">
                    <label className="label cursor-pointer">
                        <span className="label-text">Red pill</span> 
                        <input type="radio" name="radio-10" className="radio checked:bg-red-500" checked />
                    </label>
                    </div>
                    <div className="form-control">
                    <label className="label cursor-pointer">
                        <span className="label-text">Blue pill</span> 
                        <input type="radio" name="radio-10" className="radio checked:bg-blue-500" checked />
                    </label>
                </div>
            </div>
            <div className="w-full card border border-base-300">
                <input type="file" className="file-input file-input-bordered w-full max-w-xs" />
            </div>
            <div className="w-full card border border-base-300">
                <div className="form-control">
                    <label className="label cursor-pointer">
                        <span className="label-text">Remember me</span> 
                        <input type="checkbox" checked="checked" className="checkbox checkbox-primary" />
                    </label>
                </div>
            </div>
            <div className="w-full card border border-base-300">
                <input type="checkbox" className="toggle" checked />
                <input type="checkbox" className="toggle toggle-primary" checked />
                <input type="checkbox" className="toggle toggle-secondary" checked />
            </div>
        </div>
    </div>
  )
}
