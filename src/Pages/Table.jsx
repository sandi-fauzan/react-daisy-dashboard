import ExampleModal from "../components/Modal/ExampleModal";
import PerfectScrollbar from 'react-perfect-scrollbar'
import 'react-perfect-scrollbar/dist/css/styles.css';

const Table = () => {
return <>
    <div >
        <table className="table">
            {/* head */}
            <thead className="bg-base-content overflow-x-hidden text-base-100">
                <tr>
                    <th>
                        <label>
                            <input type="checkbox" className="checkbox" />
                        </label>
                    </th>
                    <th>Name</th>
                    <th>Job</th>
                    <th>Favorite Color</th>
                    <th></th>
                </tr>
            </thead>
            <tbody className="overflow-x-hidden">
                {/* row 1 */}
                {Array.from({ length: 10 }, (_, index) => (
                <tr key={index}>
                    <th>
                        <label>
                            <input type="checkbox" className="checkbox" />
                        </label>
                    </th>
                    <td>
                        <div className="flex items-center gap-3">
                            <div className="avatar">
                                <div className="mask mask-squircle flex justify-center items-center p-3 m-0 w-12 h-12">
                                    <i class="fa-solid fa-user-tie"></i>
                                </div>
                            </div>
                            <div>
                                <div className="font-bold">Hart Hagerty</div>
                                <div className="text-sm opacity-50">United States</div>
                            </div>
                        </div>
                    </td>
                    <td>
                        Zemlak, Daniel and Leannon
                        <br />
                        <span className="badge badge-ghost badge-sm">Desktop Support Technician</span>
                    </td>
                    <td>Purple</td>
                    <th>
                        <button className="btn text-[10px] mx-1 btn-neutral" onClick={()=>document.getElementById('my_modal_3').showModal()}>Neutral</button>
                        <button className="btn text-[10px] mx-1 btn-primary">Primary</button>
                        <button className="btn text-[10px] mx-1 btn-secondary">Secondary</button>
                        <button className="btn text-[10px] mx-1 btn-accent">Accent</button>
                        <div className="dropdown">
                        <div tabIndex={0} role="button" className="btn m-1"><i class="fa-solid fa-ellipsis-vertical"></i></div>
                            <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
                                <li><a>Item 1</a></li>
                                <li><a>Item 2</a></li>
                            </ul>
                        </div>
                    </th>
                </tr>
                ))}
            </tbody>

        </table>

        <div className="join">
            <button className="join-item btn">1</button>
            <button className="join-item btn">2</button>
            <button className="join-item btn btn-disabled">...</button>
            <button className="join-item btn">99</button>
            <button className="join-item btn">100</button>
        </div>

        <ExampleModal />
    </div>
</>
}

export default Table;